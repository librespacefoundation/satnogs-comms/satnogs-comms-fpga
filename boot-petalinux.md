## Boot PetaLinux

First of all you need to read the latest [user manual](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc#rendered-documents). You need to prepare all cables and have the support devices avalaible.

You need to have ready the set up that appears on figure 4.4 on user manual. Additional, you need to connect FPGA debug cable to SatNOGS COMMS. On that cable you connect a USV to UART adapter and the RGMII ethernet PHY by following the user manual.

Power up SatNOGS COMMS that runs software with [commit hash a4402503d57573ad4a93be2abe5c332fff466af8](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu) and later.
Then by using [YAMCS](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-yamcs) command the board to connect eMMC to FPGA and then enable FPGA.
Now you are ready to boot PetaLInux via JTAG. Make sure that you forward the JTAG cable to your virtual machine.
```console
petalinux-boot --jtag --kernel
```

By using ```screen``` or other related program connect to USB to UART adapter.

```console
screen /dev/ttyUSB0 115200
```

During U-Boot:

```console
U-Boot 2023.01 (Sep 21 2023 - 11:02:37 +0000)

CPU:   Zynq 7z020
Silicon: v3.1
DRAM:  ECC disabled 1 GiB
Core:  19 devices, 14 uclasses, devicetree: board
Flash: 0 Bytes
NAND:  0 MiB
MMC:   mmc@e0101000: 0
Loading Environment from nowhere... OK
In:    serial@e0001000
Out:   serial@e0001000
Err:   serial@e0001000
Net:
ZYNQ GEM: e000b000, mdio bus e000b000, phyaddr 0, interface rgmii-id
eth0: ethernet@e000b000
```
During PetaLinux Boot:
```console
Configuring network interfaces... macb e000b000.ethernet eth0: PHY [e000b000.ethernet-ffffffff:00] driver [Generic PHY] (irq=POLL)
macb e000b000.ethernet eth0: configuring for phy/rgmii-id link mode
udhcpc: started, v1.35.0
udhcpc: broadcasting discover
udhcpc: broadcasting discover
macb e000b000.ethernet eth0: Link is Up - 1Gbps/Full - flow control tx
IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
udhcpc: broadcasting discover
udhcpc: no lease, forking to background
done.
```
*Note*: As login use "petalinux"
