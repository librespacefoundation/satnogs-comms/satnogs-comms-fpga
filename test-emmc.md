## Test eMMC

This guide is based on: [Loading Images onto eMMC Devices](https://wiki.emacinc.com/wiki/Loading_Images_onto_eMMC_Devices)

### Creating a partition in eMMC

```console
sudo fdisk /dev/mmcblk0
```

Then ```n->p->1->default->+2000M->p->w``` you crate a partition with size 2000MB.

### Formatting the partition in eMMC with EXT4

```console
sudo mkfs.ext4 /dev/mmcblk0p1
```

Format the partion to EXT4.
Then mount the partition and create a file.

```console
sudo mkdir -p /mnt/card
sudo mount /dev/mmcblk0p1 /mnt/card
cd /mnt/card
sudo vi test_write # exit with wq
```

### Write eMMC via PetaLinux:,expected result: < real 0m 44s

```console
sudo time dd if=/dev/zero of=test_write bs=1M count=1000
sudo rm -rf test_write
```

### Read eMMC via PetaLinux, expected result: >22 MB/sec

```console
sudo hdparm -Ttv --direct /dev/mmcblk0p1
```
