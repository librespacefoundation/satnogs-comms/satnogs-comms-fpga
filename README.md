# SatNOGS COMMS FPGA

SatNOGS-COMMS is an open-source project that aims to develop a versatile telecommunications subsystem suitable for nano-satellites and Cubesats.
It is designed to be flexible and adaptable, allowing for a wide range of missions.

The firmware is based on the Zephyr-RTOS.
However, SatNOGS-COMMS architecture follows a modular approach.
All hardware control is implemented by the abstract platform-agnostic library [libsatnogs-comms](https://gitlab.com/librespacefoundation/satnogs-comms/libsatnogs-comms) allowing users to implement custom firmware on the RTOS of choice, quite easily.

The FPGA is an [Alinx AC7Z020 SoM based on AMD Zynq™ 7000 SoC XC7Z020](https://www.en.alinx.com/Product/SoC-System-on-Modules/Zynq-7000-SoC/AC7Z020.html). For more details about the interfaces of SOM, check either [SatNOGS COMMS Hardware repository](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware) or latest [ICD](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc#rendered-documents).

## Resources

* [Getting Started - PetaLinux](getting-started-petalinux.md)
* [Boot - PetaLinux](boot-petalinux.md)
* [Test RGMII interface](test-rgmii.md)
* [Test eMMC](test-emmc.md)

## Website and Contact

For more information about the project and Libre Space Foundation please visit our [site](https://libre.space/)
and our [community forums](https://community.libre.space).
You can also chat with the SatNOGS-COMMS development team at
[#satnogs-comms:matrix.org](https://riot.im/app/#/room/#satnogs-comms:matrix.org).

[![irc](https://img.shields.io/badge/Matrix-%23satnogs_comms:matrix.org-blue.svg)](https://matrix.to/#/#satnogs-comms:matrix.org)
[![irc](https://img.shields.io/badge/forum-discourse-blue.svg)](https://community.libre.space)

To contribute in this project follow [this guide](CONTRIBUTING.md).

## License

[![license](https://img.shields.io/badge/license-GPL%203.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202014--2025-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)
