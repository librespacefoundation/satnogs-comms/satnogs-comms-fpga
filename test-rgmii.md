## Test RGMII interface

Make sure that you detect ethernet PHY by reading [Basic Mode Control Register (BMCR)](https://www.ti.com/lit/ds/symlink/dp83867e.pdf?):

```console
sudo phytool print eth0/0x0
ieee-phy: id:0x2000a231

   ieee-phy: reg:BMCR(0x00) val:0x1140
      flags:          -reset -loopback +aneg-enable -power-down -isolate -aneg-restart -collision-test
      speed:          1000-full

   ieee-phy: reg:BMSR(0x01) val:0x796d
      capabilities:   -100-b4 +100-f +100-h +10-f +10-h -100-t2-f -100-t2-h
      flags:          +ext-status +aneg-complete -remote-fault +aneg-capable +link -jabber +ext-register
```

### Set up with ethtool
```
sudo ethtool -s eth0 autoneg on speed 100 duplex full
```

### Set up static ipv4 in Test PC

* Address: 192.168.1.1
* Netmask: 255.255.255.0
* Gateway: 192.168.1.1

### Set up static ipv4 on PetaLinux

```
sudo ip address flush dev eth0
sudo ip route flush dev eth0
sudo ip address add 192.168.1.10/24 brd + dev eth0
sudo ip route add 192.168.1.1 dev eth0
sudo ip route add default via 192.168.1.1 dev eth0
sudo ip address show dev eth0
```

### Run ping test, expected result: rtt avg < 0.6ms
From COMMS to Test PC
```
--- 192.168.1.1 ping statistics ---
405 packets transmitted, 405 packets received, 0% packet loss
round-trip min/avg/max = 0.272/0.503/0.772 ms
```

From Test PC to COMMS
```
--- 192.168.1.10 ping statistics ---
453 packets transmitted, 453 received, 0% packet loss, time 457480ms
rtt min/avg/max/mdev = 0.273/0.513/0.746/0.083 ms
```
### RGMII UDP test, expected result: >95Mbits/sec

1. Run in Test PC: ```iperf3 -s```
2. Run on COMMS: ```sudo iperf3 -c 192.168.1.1 -p 5201 -u -b 100m -t 600```

*Note:* Disable or configure your firewall.

### RGMII TCP test, expected result: > 93Mbits/sec Bitrate (R/W)

1. Run in Test PC: ```iperf3 -s```
2. Run on COMMS: ```sudo iperf3 -c 192.168.1.1 -p 5201```

*Note:* Disable or configure your firewall.
